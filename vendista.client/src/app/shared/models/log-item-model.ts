import { CommandModel } from "./command-model";

export class LogItemModel {
  public id: number = 0; // дата добавления в миллисекундах
  public command?: CommandModel | null
  public status: number = 0; // 0 - в процессе доставки, 1 - доставлено, 2 - не отправлено, 3 - ошибка при выполнении на сервере Vendista
}
