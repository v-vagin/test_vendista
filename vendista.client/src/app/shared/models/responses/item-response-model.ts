import { BasisResponseModel } from "./basis-response-model";

export class ItemResponseModel<T> extends BasisResponseModel {
  public item?: T;
}
