import { BasisResponseModel } from "./basis-response-model";

export class ListResponseModel<T>  extends BasisResponseModel {
	public page_number?: number;
	public items_per_page?: number;
	public items_count?: number;
	public items?: Array<T>;
}
