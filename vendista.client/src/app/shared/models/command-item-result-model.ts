export class CommandItemResultModel {
	public terminal_id?: number;
	public user_name?: string;
	public command_id?: number;
	public parameter1?: number;
	public parameter2?: number;
	public parameter3?: number;
	public parameter4?: number;
	public parameter5?: number;
	public parameter6?: number;
	public parameter7?: number;
	public parameter8?: number;
	public str_parameter1?: string;
	public str_parameter2?: string;
	public state?: number;
	public state_name?: string;
	public time_created?: string;
	public time_delivered?: string;
	public id?: number;
}
