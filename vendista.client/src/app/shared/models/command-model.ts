export class CommandModel {
	public id?: number;
	public name?: string;
	public parameter_name1?: string;
	public parameter_name2?: string;
	public parameter_name3?: string;
	public parameter_name4?: string;
	public parameter_name5?: string;
	public parameter_name6?: string;
	public parameter_name7?: string;
	public parameter_name8?: string;
	public str_parameter_name1?: string;
	public str_parameter_name2?: string;
	public parameter_default_value1?: number;
	public parameter_default_value2?: number;
	public parameter_default_value3?: number;
	public parameter_default_value4?: number;
	public parameter_default_value5?: number;
	public parameter_default_value6?: number;
	public parameter_default_value7?: number;
	public parameter_default_value8?: number;
	public str_parameter_default_value1?: string;
	public str_parameter_default_value2?: string;
	public visible?: boolean;
}
