export class SendCommandRequestModel {
  public command_id: number = 0;
  public parameter1: number = 0;
  public parameter2: number = 0;
  public parameter3: number = 0;
  public parameter4: number = 0;
  public parameter5: number = 0;
  public parameter6: number = 0;
  public parameter7: number = 0;
  public parameter8: number = 0;
  public str_parameter1?: string | null = null;
  public str_parameter2?: string | null = null;
}
