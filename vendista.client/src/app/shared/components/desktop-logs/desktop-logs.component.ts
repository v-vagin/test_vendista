import { Component, OnDestroy, OnInit } from '@angular/core';
import { AsyncPipe } from "@angular/common";
import { LogService } from '../../services/log.service';
import { LogItemModel } from '../../models/log-item-model';

@Component({
  selector: 'app-desktop-logs',
  standalone: true,
  imports: [
    AsyncPipe
  ],
  templateUrl: './desktop-logs.component.html'
})
export class DesktopLogsComponent implements OnInit, OnDestroy {

  constructor(
    public logService: LogService
  ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  public getLogDate(log: LogItemModel): any {
    let d = new Date(log.id);
    return `${d.getDay()}-${d.getMonth() + 1}-${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;
  }

  public getStatus(status: number): any {
    switch (status) {
      case 0:
        return 'В процессе';
      case 1:
        return 'Доставлено';
      case 2:
        return 'Не отправлено';
      case 3:
        return 'Ошибка';
    }
  }
}

