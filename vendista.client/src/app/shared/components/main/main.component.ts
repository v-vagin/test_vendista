import { Component } from '@angular/core';
import { MainMenuComponent } from '../main-menu/main-menu.component';
import { DesktopComponent } from '../desktop/desktop.component';

@Component({
  selector: 'app-main',
  standalone: true,
  imports: [
    MainMenuComponent,
    DesktopComponent
  ],
  templateUrl: './main.component.html'
})
export class MainComponent {

}
