import { Component } from '@angular/core';
import { DesktopCommandsComponent } from '../desktop-commands/desktop-commands.component';
import { DesktopLogsComponent } from '../desktop-logs/desktop-logs.component';

@Component({
  selector: 'app-desktop',
  standalone: true,
  imports: [
    DesktopCommandsComponent,
    DesktopLogsComponent
  ],
  templateUrl: './desktop.component.html'
})
export class DesktopComponent {

}
