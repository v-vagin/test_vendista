import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from '../../services/api.service';
import { ListResponseModel } from '../../models/responses/list-response-model';
import { TerminalModel } from '../../models/terminal-model';
import { CommandModel } from '../../models/command-model';
import { FormsModule } from '@angular/forms'; // , ReactiveFormsModule
import { SendCommandRequestModel } from '../../models/requests/send-command-request-model';
import { CommandItemResultModel } from '../../models/command-item-result-model';
import { ItemResponseModel } from '../../models/responses/item-response-model';
import { LogService } from '../../services/log.service';

@Component({
  selector: 'app-desktop-commands',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule //, ReactiveFormsModule
  ],
  providers:  [ ApiService ],
  templateUrl: './desktop-commands.component.html'
})
export class DesktopCommandsComponent implements OnInit, OnDestroy {
  public terminals: Array<number> | null = null;
  public selectedTerminalIndex: number = -1;
  private selectedTerminal: number = 0;

  public commands: Array<CommandModel> | null = null;
  public selectedCommandId: number = 0;
  public selectedCommand?: CommandModel | null = null;

  public isDropVisible: boolean = false;

  constructor(
    private api: ApiService,
    private log: LogService
  ) {
  }

  ngOnInit(): void {
    this.api.getTerminals()
      .then(x => this.getTerminals(x))
      .catch(x => {
        this.terminals = [];
        this.selectedTerminalIndex = -1;
      });
    this.api.getCommands()
      .then(x => this.getCommands(x))
      .catch(x => {
        this.commands = [ { id: 0, name: 'Не выбрано' } ];
        this.selectedCommandId = 0;
        this.selectedCommand = null;
      });
  }

  ngOnDestroy(): void {
    this.terminals = null;
    this.commands = null;
  }

  /**
   * Получить список терминалов
   * @returns void
   */
  private getTerminals(response: ListResponseModel<TerminalModel>): void {
    let i = 0;
    this.terminals = [];

    if (!(response?.success ?? false)) {
      console.error(response.error);
      return;
    }

    response?.items?.forEach(item => {
      let id = item.id ?? 0;

      if (id != 0) {
        this.terminals![i] = id;
        i++;
      }
    });

    if (this.terminals.length == 0) {
      return;
    }

    this.selectTerminal(0);
  }

  /**
   * Получить список команд
   * @returns void
   */
  private getCommands(response: ListResponseModel<CommandModel>): void {
    let i = 1;
    this.commands = [
      { id: 0, name: 'Не выбрано' }
    ];

    if (!(response?.success ?? false)) {
      console.error(response.error);
      return;
    }

    response?.items?.forEach(item => {
      let id = item.id ?? 0;
      let isVisible = item.visible ?? false;
      let name = item.name;

      if (id != 0 && isVisible && name != null) {
        this.commands![i] = item;
        i++;
      }
    });
  }

  /**
   * Показать список команд текущего (выбранного) терминала
   * @returns void
   */
  public showCommandsList(): void {
    if (this.selectedTerminal == 0 || (this.commands?.length ?? 0) == 0) {
      return;
    }

    this.isDropVisible = true;
  }

  /**
   * Скрыть список команд
   * @returns void
   */
  public hideCommandsList(): void {
    this.isDropVisible = false;
  }

  /**
   * Выбор терминала по его индексу из списка терминалов
   * @param i индекс терминала в списке терминалов
   * @returns void
   */
  public selectTerminal(i: number): void {
    if (this.terminals == null || this.terminals.length == 0 || i > this.terminals.length - 1) {
      return;
    }

    this.hideCommandsList();
    this.selectedTerminalIndex = i;
    this.selectedTerminal = this.terminals[i];
  }

  /**
   * Выбор команды по её индексу из списка команд
   * @param i индекс команд в списке команд
   * @returns void
   */
  public selectCommand(i: number): void {
    if (this.commands == null || this.commands.length == 0) {
      return;
    }

    let id: number = this.commands[i].id ?? -1;
    this.selectedCommandId = id;

    if (id < 0) {
      console.error('Incorrect command id.');
      return;
    }

    this.selectedCommand = this.commands[i];

    this.hideCommandsList();
  }

  public sendCommand(): void {
    let data: SendCommandRequestModel = {
      command_id: this.selectedCommandId,
      parameter1: this.selectedCommand?.parameter_default_value1 ?? 0,
      parameter2: this.selectedCommand?.parameter_default_value2 ?? 0,
      parameter3: this.selectedCommand?.parameter_default_value3 ?? 0,
      parameter4: this.selectedCommand?.parameter_default_value4 ?? 0,
      parameter5: this.selectedCommand?.parameter_default_value5 ?? 0,
      parameter6: this.selectedCommand?.parameter_default_value6 ?? 0,
      parameter7: this.selectedCommand?.parameter_default_value7 ?? 0,
      parameter8: this.selectedCommand?.parameter_default_value8 ?? 0,
      str_parameter1: this.selectedCommand?.str_parameter_default_value1,
      str_parameter2: this.selectedCommand?.str_parameter_default_value2
    };

    let id = Date.now();
    this.log.addLog({
      id: id,
      command: this.selectedCommand,
      status: 0
    });
    this.api.sendCommand(this.selectedTerminal, data)
      .then((response: ItemResponseModel<CommandItemResultModel>) => {
        if (!(response?.success ?? false)) {
          console.error(response.error);
          this.log.changeStatus(id, 3);
          return;
        }
        this.log.changeStatus(id, 1);
      })
      .catch(x => {
        this.log.changeStatus(id, 2);
      });

    this.selectedCommandId = 0;
    this.selectedCommand = null;
  }
}
