import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { TerminalModel } from '../models/terminal-model';
import { ListResponseModel } from '../models/responses/list-response-model';
import { CommandModel } from '../models/command-model';
import { SendCommandRequestModel } from '../models/requests/send-command-request-model';
import { CommandItemResultModel } from '../models/command-item-result-model';
import { ItemResponseModel } from '../models/responses/item-response-model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiServer: string = 'https://localhost:7034/api';

  constructor(
    private http: HttpClient
  ) {
  }

  /**
   * Получить список терминалов
   * @returns Список терминалов
   */
  public async getTerminals(): Promise<any> {
    let request: string = `${this.apiServer}/getTerminals`;
    return await firstValueFrom(this.http.get<ListResponseModel<TerminalModel>>(request));
  }

  /**
   * Получить список команд
   * @returns Список команд
   */
  public async getCommands(): Promise<any> {
    let request: string = `${this.apiServer}/getCommands`;
    return await firstValueFrom(this.http.get<ListResponseModel<CommandModel>>(request));
  }

  /**
   * Отправить команду на терминал
   * @returns Результат отправки
   */
  public async sendCommand(terminal_id: number, data: SendCommandRequestModel): Promise<any> {
    let url: string = `${this.apiServer}/sendCommand/${terminal_id}`;
    return await firstValueFrom(this.http.post<ItemResponseModel<CommandItemResultModel>>(url, data));
  }
}
