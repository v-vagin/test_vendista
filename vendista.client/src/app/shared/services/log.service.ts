import { Injectable } from '@angular/core';
import { LogItemModel } from '../models/log-item-model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  public logs: Array<LogItemModel> = [];
  public logUpdated$: Subject<Array<LogItemModel>> = new Subject();
  private logCounter: number = 0;

  constructor() { }

  public addLog(item: LogItemModel): void {
    this.logs.push(item);
    // this.logCounter++;
    // this.logUpdated$.next(this.logCounter);
  }

  public changeStatus(id: number, status: number) {
    var i = this.logs.findIndex(x => x.id == id);

    if (i < 0) {
      return;
    }

    this.logs[i].status = status;
    this.logCounter++;
    this.logUpdated$.next(this.logs);
  }
}
