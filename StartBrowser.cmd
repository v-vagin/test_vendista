@echo off

if exist "%ProgramFiles%\Google\Chrome\Application\chrome.exe" goto RunChrome
if exist "%LOCALAPPDATA%\Yandex\YandexBrowser\Application\browser.exe" goto RunYandex
if exist "%ProgramFiles%\Mozilla Firefox\firefox.exe" goto RunFirefox
if exist "%ProgramFiles(x86)%\Microsoft\Edge\Application\msedge.exe" goto RunEdge
if exist "%LOCALAPPDATA%\Mail.Ru\Atom\Application\atom.exe" goto RunAtom
echo Browser not found. Launch the browser manually.
exit

:RunChrome
"%ProgramFiles%\Google\Chrome\Application\chrome.exe" https://localhost:4200/
exit

:RunYandex
"%LOCALAPPDATA%\Yandex\YandexBrowser\Application\browser.exe" https://localhost:4200/
exit

:RunFirefox
"%ProgramFiles%\Mozilla Firefox\firefox.exe" https://localhost:4200/
exit

:RunEdge
"%ProgramFiles(x86)%\Microsoft\Edge\Application\msedge.exe" https://localhost:4200/
exit

:RunAtom
"%LOCALAPPDATA%\Mail.Ru\Atom\Application\atom.exe" https://localhost:4200/
exit
