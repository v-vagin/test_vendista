using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Net;
using Vendista.Server.Models;
using Vendista.Server.Models.Requests;
using Vendista.Server.Models.Responses;
using Vendista.Server.Services;

namespace Vendista.Server.Controllers;

[ApiController]
[Route("[controller]")]
public class ApiController : ControllerBase {
	private readonly ILogger<ApiController> _logger;
	private readonly IBasisService _basisService;

	public ApiController(ILogger<ApiController> logger, IBasisService basisService) {
		_logger = logger;
		_basisService = basisService;
	}

	[HttpGet("hello")]
	public IActionResult Hello() {
		return Ok("Hello");
	}

	[HttpGet("getTerminals")]
	public async Task<IActionResult> GetTerminals() {
		await _basisService.GetAuthToken();
		var result = await _basisService.GetTerminals();
		return result.StatusCode switch {
			HttpStatusCode.OK => Ok(result.Data),
			_ => Ok(new BasisResponseModel() { Error = result.Error })
		};
	}

	[HttpGet("getCommands")]
	public async Task<IActionResult> GetCommands() {
		await _basisService.GetAuthToken();
		var result = await _basisService.GetCommands();
		return result.StatusCode switch {
			HttpStatusCode.OK => Ok(result.Data),
			_ => Ok(new BasisResponseModel() { Error = result.Error })
		};
	}

	[HttpPost("sendCommand/{id}")]
	public async Task<IActionResult> SendCommand([Required] int id, [FromBody] SendCommandRequestModel request) {
		if (id == 0 || (request?.CommandId ?? 0) == 0) {
			return StatusCode((int)HttpStatusCode.BadRequest, new BasisResponseModel() { Error = "Incorrect input data." });
		}

		await _basisService.GetAuthToken();
		var result = await _basisService.SendCommand(id, request!);
		return result.StatusCode switch {
			HttpStatusCode.OK => Ok(result.Data),
			_ => Ok(new BasisResponseModel() { Error = result.Error })
		};
	}
}
