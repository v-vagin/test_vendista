﻿using Microsoft.Net.Http.Headers;
using System.Net.Mime;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Text;
using Vendista.Server.Enums;
using Vendista.Server.Models.Responses;
using System.Net;
using Vendista.Server.Models;
using Vendista.Server.Controllers;
using Microsoft.AspNetCore.Mvc;
using Vendista.Server.Models.Requests;
using Microsoft.AspNetCore.Http;
using static System.Runtime.InteropServices.JavaScript.JSType;
using Microsoft.AspNetCore.Http.HttpResults;

namespace Vendista.Server.Services;

public interface IBasisService {
	/// <summary>
	/// Получить токен авторизации.
	/// </summary>
	Task GetAuthToken();
	/// <summary>
	/// Получить список терминалов
	/// </summary>
	/// <returns>Список терминалов</returns>
	Task<ServiceResponseModel<ListResponseModel<TerminalModel>>> GetTerminals();
	/// <summary>
	/// Получить список команд
	/// </summary>
	/// <returns>Список команд</returns>
	Task<ServiceResponseModel<ListResponseModel<CommandModel>>> GetCommands();
	/// <summary>
	/// Отправить команду на терминал
	/// </summary>
	/// <returns>Результат отправки</returns>
	Task<ServiceResponseModel<ItemResponseModel<CommandItemResultModel>>> SendCommand(int id, SendCommandRequestModel data);
}

public class BasisService : IBasisService {
	private readonly ILogger<BasisService> _logger;
	private readonly RepositoryService _repository;
	private string? _token;

	public BasisService(ILogger<BasisService> logger, RepositoryService repository) {
		_logger = logger;
		_repository = repository;
	}

	/// <summary>
	/// Метод для отправки HTTP запроса и получения ответа.
	/// </summary>
	/// <typeparam name="T">
	/// Базовый тип (string, int и т.д.) отправляемых даных при GET запросе или
	/// модель отправляемых даных при POST запросе.
	/// </typeparam>
	/// <param name="url">URL сервера для отправки запроса.</param>
	/// <param name="requestType">Тип <see cref="RequestTypeEnum"/> HTTP запроса, POST по умолчанию.</param>
	/// <param name="data">Данные для отправки в POST запросе.</param>
	/// <param name="contentEncodingType">
	/// Тип <see cref="EncodingTypeEnum"/> кодировки для содержимого, по умолчанию <see cref="EncodingTypeEnum.Utf8"/>
	/// </param>
	/// <param name="headers">Список дополнительных заголовков, которые необходимо добавить в HTTP запросе.</param>
	/// <param name="isLongPollRequest">true - если необходимо отправить запрос с долгим временем ожидания.</param>
	/// <returns>
	/// Модель данных с результатом выполнения HTTP запроса <see cref="HttpResponseModel"/>.
	/// </returns>
	public static async Task<HttpResponseModel> SendHttpRequest<T>(string url,
		RequestTypeEnum requestType = RequestTypeEnum.Post,
		T? data = default,
		EncodingTypeEnum contentEncodingType = EncodingTypeEnum.Utf8,
		List<(string Header, string HeaderData)>? headers = null,
		bool isLongPollRequest = false)
	{
		if (string.IsNullOrWhiteSpace(url)) {
			return new(HttpStatusCode.BadRequest, "Request URL is empty.");
		}

		var statusCode = HttpStatusCode.OK;
		try {
			HttpRequestMessage request;
			using var client = new HttpClient();

			if (isLongPollRequest) {
				client.Timeout = TimeSpan.FromMilliseconds(Timeout.Infinite);
			}

			switch (requestType) {
				case RequestTypeEnum.Get: {
					request = new(HttpMethod.Get, url);
					break;
				}

				default: {
					string json = JsonSerializer.Serialize(data, new JsonSerializerOptions {
						PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
						DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
					});
					request = new(HttpMethod.Post, url) {
						Content = contentEncodingType switch {
							EncodingTypeEnum.Ascii => new StringContent(json, Encoding.ASCII, MediaTypeNames.Application.Json),
							EncodingTypeEnum.Latin1 => new StringContent(json, Encoding.Latin1, MediaTypeNames.Application.Json),
							EncodingTypeEnum.Unicode => new StringContent(json, Encoding.Unicode, MediaTypeNames.Application.Json),
							EncodingTypeEnum.Utf8 => new StringContent(json, Encoding.UTF8, MediaTypeNames.Application.Json),
							EncodingTypeEnum.Utf32 => new StringContent(json, Encoding.UTF32, MediaTypeNames.Application.Json),
							_ => new StringContent(json, Encoding.Default, MediaTypeNames.Application.Json)
						}
					};
					_ = request.Headers.TryAddWithoutValidation(HeaderNames.ContentType, MediaTypeNames.Application.Json);
					break;
				}
			}

			_ = request.Headers.TryAddWithoutValidation(HeaderNames.Accept, MediaTypeNames.Application.Json);
			_ = request.Headers.TryAddWithoutValidation(HeaderNames.AcceptCharset, "charset=utf-8");

			if ((headers?.Count ?? 0) != 0) {
				foreach (var item in headers!) {
					if (string.IsNullOrWhiteSpace(item.Header)) {
						continue;
					}

					_ = request.Headers.TryAddWithoutValidation(item.Header, item.HeaderData);
				}
			}

			using var response = await client.SendAsync(request, HttpCompletionOption.ResponseContentRead /*.ResponseHeadersRead*/);
			var content = await response.Content.ReadAsStringAsync();

			if (!response.IsSuccessStatusCode) {
				statusCode = response.StatusCode;
				throw new Exception(content);
			}

			return new(response.StatusCode, content);
		}
		catch (Exception ex) {
			return new(statusCode, ex.Message);
		}
	}

	/// <summary>
	/// Получить токен авторизации.
	/// </summary>
	public async Task GetAuthToken() {
		var url = $"{_repository.ApiUrl}/token?login={_repository.ApiLogin}&password={_repository.ApiPassword}";
		var response = await SendHttpRequest<string>(url, RequestTypeEnum.Get);

		if (response.StatusCode != HttpStatusCode.OK) {
			_logger.LogError("GetAuthToken: [{StatusCode}] - {Data}", response.StatusCode, response.Data);
			_token = null;
			return;
		}

		if (string.IsNullOrWhiteSpace(response.Data)) {
			_logger.LogError("GetAuthToken: Empty response.");
			_token = null;
			return;
		}

		try {
			var result = JsonSerializer.Deserialize<TokenResponseModel>(response.Data);

			if (result == null || string.IsNullOrWhiteSpace(result.Token)) {
				_logger.LogError("GetAuthToken: Empty token.");
				_token = null;
				return;
			}

			_token = result.Token;
		}
		catch (Exception ex) {
			_logger.LogError("GetAuthToken: {Error}", ex.Message);
			_token = null;
		}

	}

	private (HttpStatusCode Status, string? Error, T? Data) ParseResponse<T>(string method, HttpResponseModel? response) {
		if (response == null || string.IsNullOrWhiteSpace(response.Data)) {
			return (HttpStatusCode.InternalServerError, $"{method}: Empty response from Vendista API server.", default);
		}

		try {
			if (response.StatusCode != HttpStatusCode.OK) {
				var result = JsonSerializer.Deserialize<T>(response.Data) as BasisResponseModel;
				return (response.StatusCode, result?.Error, default);
			}

			return (HttpStatusCode.OK, null, JsonSerializer.Deserialize<T>(response.Data));
		}
		catch (Exception ex) {
			var message = $"{method}: Parsing error - {ex.Message}";
			_logger.LogError(message);
			return (HttpStatusCode.InternalServerError, message, default);
		}

	}

	/// <summary>
	/// Получить список терминалов
	/// </summary>
	/// <returns>Список терминалов</returns>
	public async Task<ServiceResponseModel<ListResponseModel<TerminalModel>>> GetTerminals() {
		if (string.IsNullOrEmpty(_token)) {
			return new ServiceResponseModel<ListResponseModel<TerminalModel>>(HttpStatusCode.Unauthorized, "Empty token.", null);
		}

		var url = $"{_repository.ApiUrl}/terminals?token={_token}";
		var response = await SendHttpRequest<string>(url, RequestTypeEnum.Get);
		var result = ParseResponse<ListResponseModel<TerminalModel>>("GetTerminals", response);
		return new ServiceResponseModel<ListResponseModel<TerminalModel>>(result.Status, result.Error, result.Data);
	}

	/// <summary>
	/// Получить список команд
	/// </summary>
	/// <returns>Список команд</returns>
	public async Task<ServiceResponseModel<ListResponseModel<CommandModel>>> GetCommands() {
		if (string.IsNullOrEmpty(_token)) {
			return new ServiceResponseModel<ListResponseModel<CommandModel>> (HttpStatusCode.Unauthorized, "Empty token.", null);
		}

		var url = $"{_repository.ApiUrl}/commands/types?token={_token}";
		var response = await SendHttpRequest<string>(url, RequestTypeEnum.Get);
		var result = ParseResponse<ListResponseModel<CommandModel>>("GetCommands", response);
		return new ServiceResponseModel<ListResponseModel<CommandModel>>(result.Status, result.Error, result.Data);
	}

	/// <summary>
	/// Отправить команду на терминал
	/// </summary>
	/// <returns>Результат отправки</returns>
	public async Task<ServiceResponseModel<ItemResponseModel<CommandItemResultModel>>> SendCommand(int id, SendCommandRequestModel data) {
		if (string.IsNullOrEmpty(_token)) {
			return new ServiceResponseModel<ItemResponseModel<CommandItemResultModel>>(HttpStatusCode.Unauthorized, "Empty token.", null);
		}

		var url = $"{_repository.ApiUrl}/terminals/{id}/commands?token={_token}";
		var response = await SendHttpRequest(url, RequestTypeEnum.Post, data);
		var result = ParseResponse<ItemResponseModel<CommandItemResultModel>>("SendCommand", response);
		return new ServiceResponseModel<ItemResponseModel<CommandItemResultModel>>(result.Status, result.Error, result.Data);
	}
}
