﻿namespace Vendista.Server.Services;

public interface IRepositoryService {
	void GetSettings(IConfiguration builder);
}

public class RepositoryService : IRepositoryService {
	private const string VendistaApiUrl = "VendistaApiUrl";
	private const string VendistaApiLogin = "VendistaApiLogin";
	private const string VendistaApiPassword = "VendistaApiPassword";

	private IConfiguration? _settings;

	public string? ApiUrl { get; protected set; }
	public string? ApiLogin { get; protected set; }
	public string? ApiPassword { get; protected set; }

	public void GetSettings(IConfiguration builder) {
		_settings = builder;
		ApiUrl = _settings?.GetValue<string>(VendistaApiUrl) ?? null;
		ApiLogin = _settings?.GetValue<string>(VendistaApiLogin) ?? null;
		ApiPassword = _settings?.GetValue<string>(VendistaApiPassword) ?? null;
	}
}
