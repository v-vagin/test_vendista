﻿using System.Text.Json.Serialization;

namespace Vendista.Server.Models;

public class TerminalModel {
	[JsonPropertyName("bank_id")]
	public string? BankId { get; set; }

	[JsonPropertyName("serial_number")]
	public string? SerialNumber { get; set; }

	[JsonPropertyName("version")]
	public int Version { get; set; }

	[JsonPropertyName("fw_type")]
	public int FwType { get; set; }

	[JsonPropertyName("gsm_operator")]
	public string? GsmOperator { get; set; }

	[JsonPropertyName("gsm_rssi")]
	public int GsmRssi { get; set; }

	[JsonPropertyName("imei")]
	public long Imei { get; set; }

	[JsonPropertyName("partner_id")]
	public int PartnerId { get; set; }

	[JsonPropertyName("partner_name")]
	public string? PartnerName { get; set; }

	[JsonPropertyName("external_server_id")]
	public int? ExternalServerId { get; set; }

	[JsonPropertyName("last_online_time")]
	public string? LastOnlineTime { get; set; }

	[JsonPropertyName("last24_hours_online")]
	public int Last24HoursOnline { get; set; }

	[JsonPropertyName("last_hour_online")]
	public int LastHourOnline { get; set; }

	[JsonPropertyName("sber_qrid")]
	public string? SberQrId { get; set; }

	[JsonPropertyName("auto_cancel_timeout")]
	public int AutoCancelTimeout { get; set; }

	[JsonPropertyName("bonus_percent")]
	public float? BonusPercent { get; set; }

	[JsonPropertyName("qr_discount_percent")]
	public float QrDiscountPercent { get; set; }

	[JsonPropertyName("send_cash")]
	public bool SendCash { get; set; }

	[JsonPropertyName("send_cashless")]
	public bool SendCashless { get; set; }

	[JsonPropertyName("type")]
	public int Type { get; set; }

	[JsonPropertyName("sim_balance")]
	public int SimBalance { get; set; }

	[JsonPropertyName("sim_number")]
	public long? SimNumber { get; set; }

	[JsonPropertyName("paid_sim")]
	public bool PaidSim { get; set; }

	[JsonPropertyName("division_id")]
	public int? DivisionId { get; set; }

	[JsonPropertyName("bootloader_version")]
	public int BootloaderVersion { get; set; }

	[JsonPropertyName("min_pay_server")]
	public int MinPayServer { get; set; }

	[JsonPropertyName("success_message")]
	public string? SuccessMessage { get; set; }

	[JsonPropertyName("success_message_timeout")]
	public int SuccessMessageTimeout { get; set; }

	[JsonPropertyName("machine_id")]
	public int MachineId { get; set; }

	[JsonPropertyName("ping")]
	public int Ping { get; set; }

	[JsonPropertyName("preffered_port")]
	public int? PrefferedPort { get; set; }

	[JsonPropertyName("disable_firmware_updates")]
	public bool DisableFirmwareUpdates { get; set; }

	[JsonPropertyName("comment")]
	public string? Comment { get; set; }

	[JsonPropertyName("owner_id")]
	public int OwnerId { get; set; }

	[JsonPropertyName("longitude")]
	public double Longitude { get; set; }

	[JsonPropertyName("latitude")]
	public double Latitude { get; set; }

	[JsonPropertyName("owner_name")]
	public string? OwnerName { get; set; }

	[JsonPropertyName("color")]
	public string? Color { get; set; }

	[JsonPropertyName("sum")]
	public int Sum { get; set; }

	[JsonPropertyName("id")]
	public int Id { get; set; }

}
