﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Vendista.Server.Models.Requests;

public class SendCommandRequestModel {
	[JsonPropertyName("command_id")]
	[Required]
	public int CommandId { get; set; }

	[JsonPropertyName("parameter1")]
	[Required]
	public int Parameter1 { get; set; }

	[JsonPropertyName("parameter2")]
	[Required]
	public int Parameter2 { get; set; }

	[JsonPropertyName("parameter3")]
	[Required]
	public int Parameter3 { get; set; }

	[JsonPropertyName("parameter4")]
	[Required]
	public int Parameter4 { get; set; }

	[JsonPropertyName("parameter5")]
	[Required]
	public int Parameter5 { get; set; }

	[JsonPropertyName("parameter6")]
	[Required]
	public int Parameter6 { get; set; }

	[JsonPropertyName("parameter7")]
	[Required]
	public int Parameter7 { get; set; }

	[JsonPropertyName("parameter8")]
	[Required]
	public int Parameter8 { get; set; }

	[JsonPropertyName("str_parameter1")]
	public string? StrParameter1 { get; set; }

	[JsonPropertyName("str_parameter2")]
	public string? StrParameter2 { get; set; }
}
