﻿using System.Text.Json.Serialization;

namespace Vendista.Server.Models.Responses;

public class ItemResponseModel<T> : BasisResponseModel {
	[JsonPropertyName("item")]
	public T? Item { get; set; }
}
