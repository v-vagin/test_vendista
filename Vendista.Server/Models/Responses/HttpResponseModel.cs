﻿using System.Net;

namespace Vendista.Server.Models.Responses;

/// <summary>
/// Модель для возврата результата выполнения HTTP запроса.
/// </summary>
/// <param name="StatusCode">
/// Код состояния ответа на запрос.
/// </param>
/// <param name="Data">
/// Ответ сервера в строковом виде для последующей де-сериализации,
/// либо сообщение об ошибке, если StatusCode != <see cref="HttpStatusCode.OK"/>.
/// </param>
public record HttpResponseModel(HttpStatusCode StatusCode, string Data);
