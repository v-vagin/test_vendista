﻿using System.Net;

namespace Vendista.Server.Models.Responses;

/// <summary>
/// Модель для возврата результата выполнения операции сервисом.
/// </summary>
/// <param name="StatusCode">
/// Код состояния выполненной операции.
/// </param>
/// <param name="Error">
/// Сообщение об ошибке.
/// </param>
/// <param name="Data">
/// Данные с результатом выполнения.
/// </param>
public record ServiceResponseModel<T>(HttpStatusCode StatusCode, string? Error, T? Data);
