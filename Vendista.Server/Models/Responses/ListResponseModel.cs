﻿using System.Text.Json.Serialization;

namespace Vendista.Server.Models.Responses;

public class ListResponseModel<T> : BasisResponseModel {
	[JsonPropertyName("page_number")]
	public int PageNumber { get; set; }

	[JsonPropertyName("items_per_page")]
	public int ItemsPerPage { get; set; }

	[JsonPropertyName("items_count")]
	public long ItemsCount { get; set; }

	[JsonPropertyName("items")]
	public List<T>? Items { get; set; }
}
