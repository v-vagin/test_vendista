﻿using System.Text.Json.Serialization;

namespace Vendista.Server.Models.Responses;

public class BasisResponseModel {
	[JsonPropertyName("success")]
	public bool Success { get; set; } = false;

	[JsonPropertyName("error")]
	public string? Error { get; set; }
}
