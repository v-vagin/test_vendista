﻿using System.Text.Json.Serialization;

namespace Vendista.Server.Models.Responses;

public class TokenResponseModel : BasisResponseModel {
	[JsonPropertyName("token")]
	public string? Token { get; set; }

	[JsonPropertyName("owner_id")]
	public int OwnerId { get; set; }

	[JsonPropertyName("role_id")]
	public int RoleId { get; set; }

	[JsonPropertyName("name")]
	public string? Name { get; set; }

	[JsonPropertyName("user_id")]
	public int UserId { get; set; }

	[JsonPropertyName("time_zone")]
	public string? TimeZone { get; set; }
}
