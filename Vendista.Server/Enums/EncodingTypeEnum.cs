﻿namespace Vendista.Server.Enums;

public enum EncodingTypeEnum {
	Default = 0,
	Ascii,
	Latin1,
	Unicode,
	Utf8,
	Utf32
}