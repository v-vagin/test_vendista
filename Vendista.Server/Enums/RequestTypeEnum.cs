﻿namespace Vendista.Server.Enums;

public enum RequestTypeEnum {
	Get,
	Post
}
