using System.Globalization;
using System.Text;
using Vendista.Server.Services;

namespace Vendista.Server;

public class Program {
	public static void Main(string[] args) {
		Console.OutputEncoding = Encoding.UTF8;
		CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;

		var builder = WebApplication.CreateBuilder(args);

		// Add services to the container.
		builder.Services.AddControllers();
		// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
		builder.Services.AddEndpointsApiExplorer();
		builder.Services.AddSwaggerGen();

		builder.Services.AddSingleton<RepositoryService>();
		builder.Services.AddTransient<IBasisService, BasisService>();

		builder.Services.AddCors(options => {
			options.AddPolicy("AllowAllPolicy",
				x => x.AllowAnyHeader()
					.AllowAnyMethod()
					.AllowCredentials()
					//.SetIsOriginAllowed(y => true)
					.WithOrigins([
						"http://localhost:4200",
						"https://localhost:4200",
						"http://127.0.0.1:4200",
						"https://127.0.0.1:4200",
					])
			);
		});

		var app = builder.Build();

		using var scope = app.Services.CreateScope();
		var repo = scope.ServiceProvider.GetService<RepositoryService>();
		repo!.GetSettings(app.Configuration);
		scope.Dispose();

		app.UseDefaultFiles();
		app.UseStaticFiles();
		app.UseCors("AllowAllPolicy");

		// Configure the HTTP request pipeline.
		if (app.Environment.IsDevelopment()) {
			app.UseSwagger();
			app.UseSwaggerUI();
		}

		app.UseHttpsRedirection();
		app.UseAuthorization();

		app.MapControllers();

		app.MapFallbackToFile("/index.html");

		app.Run();
	}
}
